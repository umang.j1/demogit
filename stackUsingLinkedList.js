
class Node{
	constructor(element){
		this.element=element;
		this.next=null;
	}
}

class stack{
	constructor(){
		this.head=null;
		this.size=0;
	}

	isEmpty(){
		if (this.size==0){
			// console.log("true");
			return true;
		}
		return false;
	}

	peek(){
		if (!this.isEmpty()){
			return this.head.element;
		}
		else{
			return "no element!";
		}
	}

	//push element
	push(element){
		var newNode = new Node(element);
		var temp;
		if (this.size===0){
			this.head=newNode;
		}
		else{
			// while (temp.next){
			// 	temp=temp.next;
			// }

			// temp.next=newNode;
			// console.log(newNode);
			newNode.next=this.head;
			this.head=newNode;

		}
		console.log(element,"is inserted!")
		this.size++;

	}

	pop(){
		if (this.isEmpty()){
			console.log("no element to pop!")
		}
		else{
			var temp = this.head;
			this.head=this.head.next;
			temp.next=null;
			return temp.element;
		}
	}

	printAllNode(){
		var temp=this.head;
		while(temp){
			console.log(temp.element);
			temp=temp.next;
		}
	}
}

var s = new stack();
s.printAllNode();
console.log(s.isEmpty())
s.push(2);
console.log(s.isEmpty())
s.push(4);
s.push(6);
console.log(s.pop(),"is deleted!");
s.push(8);
s.push(10);
s.peek()
console.log("peek element",s.peek());
console.log(s.pop(),"is deleted!");

s.printAllNode();