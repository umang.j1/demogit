promise1=new Promise((res,rej) => {rej("rejection1");})
promise2=new Promise((res,rej) => {rej("rejection2");})
promise3=new Promise((res,rej) => {res("done resolve");})
Promise.all([promise1,promise2,promise3])
	.then((s)=>console.log(s))
	.catch((err)=>console.log(err));
