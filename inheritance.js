class person{
	constructor(name,age,position){
		this.name=name;
		this.age=age;
		this.position=position;
	}

	greeting(){
		console.log("hello",this.name);
		return this.name;
	}

	farewell(){
		console.log("bye bye",this.name);
		return;
	}
}

class employee extends person{
	constructor(name,age,position,_subject,grade){
		super(name,age,position);
		this._subject=_subject;
		this.grade=grade;
	}

	get subject(){
		return this._subject;
	}

	set subject(sub){
		// console.log("inside setter");
		this._subject="The subject is = " + sub;
	}


	greeting(){
		console.log("hello",super.greeting(),this.grade,"having subject",this._subject);
	}
}

const p = new person("umang",21,"intern")
// p.greeting();
// p.farewell();
const e = new employee("abhijeet",24,"senior developer","english",98);
e.greeting();
e.subject="hindi";
e.greeting();
