const arr=[70,45,87,37,90,67,84];
const newarr=arr.filter((num) => num>=70);
console.log(newarr)
console.log(typeof newarr)



/*destructing objects*/

const umang={name:"umang",last:"joshi",links:{facebook:"fsfsdfds.facebook",insta:"dasdajsdinsta"}};
const {facebook:fab} =umang.links;
console.log(fab);

const {height:h=500,width:w=100}={width:200};
console.log(w)	;
console.log(h);

/*destructor for arrays*/

const team_A1=["Rohan","anand","abhijeet","geetanjali","melwyn","kapil","umang"];
const [manager,...employee_engineer]=team_A1;
console.log(manager);
console.log(employee_engineer);

/*string methods*/
const first_name="umang";
const last_name="joshi";
console.log(first_name.startsWith('uma'));
console.log(first_name.endsWith('ng'));
console.log(first_name.includes("man"));
console.log(first_name.repeat(4));