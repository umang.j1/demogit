/*const promise=new Promise(function (res,rej){
	res("you got : ","umang");
})

promise.then((a,b)=>console.log(a+"success"+b)).catch((err)=>console.log("failure "+err));*/

// const promise=new Promise(function(resolve,reject){
// 	let a=Math.random();
// 	if (a>0){resolve("hello this is positive: "+a);}
// 	else{reject("this is not positive")};
// })
// async function f(){
// await promise.then((res)=>console.log(res),(rej)=>console.log(rej));
// }
// f().then(() => console.log(5**20));


// const promise1 = Promise.reject(3);
// const promise2 = Promise.resolve(13);
// const promise3 = new Promise(function(resolve, reject) {
//   setTimeout(resolve, 100, 'foo');
// });
// const promise4=Promise.reject(5);
// Promise.any([promise1, promise2, promise3,promise4]).then(function(values) {
//   console.log(values);
// });	

/*promise chaining*/
// const promise1= new Promise(function(res,reject){
// 	setTimeout(()=>{res(1)},1000);
// }).then((a)=>{console.log(a);return a}).then((s1)=>{let s2=s1*2;console.log(s2);return s2})
// .then((s3)=>{console.log(s3*2);let s4=s3*2;return s4})
// .then((s4)=>setTimeout(()=>console.log(s4),1000));
const getData1 = async() => { 
	let y = await "Hello World"; 
	console.log(y); 
} 

const getData2 = async() => { 
	let y = await "Hello World 23"; 
	console.log(y); 
} 

console.log(1); 
getData2(); 
getData1();
console.log(2); 
